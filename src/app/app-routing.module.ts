import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddmovieComponent } from './movies/addmovie/addmovie.component';
import { UpdatemovieComponent } from './movies/updatemovie/updatemovie.component';
import { ViewmovieComponent } from './movies/viewmovie/viewmovie.component';

const routes: Routes = [
  {path: 'view-movies', component: ViewmovieComponent},
  {path: 'update-movies', component: UpdatemovieComponent},
  {path: 'add-movies', component: AddmovieComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
