import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {
  baseUrl : string = "https://localhost:7098/api/";

  constructor(private http: HttpClient) { }

  getAllMovies(){
    return this.http.get(this.baseUrl + 'Movie/getallmovies');
  }

  getMovieById(id: string ){
    return this.http.get(this.baseUrl + 'Movie/getmoviebyid?id=' + id);
  }

  addMovie(movie: any){
    return this.http.post(this.baseUrl + 'Movie/addmovie', movie);
  }

  deleteMovie(id: any){
    return this.http.delete(this.baseUrl + 'Movie/deletemovie?id=' + id);
  }

  updateMovie(movie: any){
    return this.http.put(this.baseUrl + 'Movie/updatemovie', movie);
  }

  getAllActors(){
    return this.http.get(this.baseUrl + 'Actor');
  }

  getAllPeoducers(){
    return this.http.get(this.baseUrl + 'Producer/getallproducers');
  }

  getAllDirectors(){
    return this.http.get(this.baseUrl + 'Director/getalldirectors');
  }
}
