import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  updateMovie: any;

  constructor() { }

  getUpdatedMovieData(){
    return this.updateMovie;
  }

  setUpdatedMovieData(data: any) {
    this.updateMovie = data;
  }
}
