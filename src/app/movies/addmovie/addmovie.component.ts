import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiserviceService } from 'src/app/services/apiservice.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-addmovie',
  templateUrl: './addmovie.component.html',
  styleUrls: ['./addmovie.component.css']
})
export class AddmovieComponent implements OnInit {

  constructor(private formbuilder: FormBuilder, private apiService: ApiserviceService) { }

  dropdownListActors:any = [];
  selectedItemsActors:any = [];
  dropdownListProducers:any = [];
  selectedItemsProducers:any = [];
  dropdownListDirectors:any = [];
  selectedItemsDirectors:any = [];
  dropdownSettings:IDropdownSettings={};

  myForm = this.formbuilder.group({
    moviename : ['', Validators.required],
    releasedate : ['', Validators.required],
    actors : ['', Validators.required],
    directors : ['', Validators.required],
    producers : ['', Validators.required],
    rating : ['', Validators.required],
    totalcost : ['', Validators.required],
    description : ['', Validators.required]
  });

  update(){
    if(this.myForm.status == 'VALID'){
      let movie = {
        movieName: this.myForm.value.moviename,
        relaseDate: this.myForm.value.releasedate,
        actors: this.myForm.value.actors,
        directors: this.myForm.value.directors,
        producers: this.myForm.value.producers,
        rating: this.myForm.value.rating,
        totalCost: this.myForm.value.totalcost,
        description: this.myForm.value.description
      }

      this.apiService.addMovie(movie).subscribe(data => console.log(data));
    }
  }

  ngOnInit() {
    this.apiService.getAllActors().subscribe(res => {
      this.dropdownListActors = res;
      console.log(this.dropdownListActors); 
    });
    this.apiService.getAllDirectors().subscribe(res => this.dropdownListDirectors = res);
    this.apiService.getAllPeoducers().subscribe(res => this.dropdownListProducers = res);
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

}
