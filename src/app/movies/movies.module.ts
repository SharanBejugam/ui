import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MoviesRoutingModule } from './movies-routing.module';
import { AddmovieComponent } from './addmovie/addmovie.component';
import { ViewmovieComponent } from './viewmovie/viewmovie.component';
import { UpdatemovieComponent } from './updatemovie/updatemovie.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';



@NgModule({
  declarations: [
    AddmovieComponent,
    ViewmovieComponent,
    UpdatemovieComponent

  ],
  imports: [
    CommonModule,
    MoviesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule.forRoot(),
  ]
})
export class MoviesModule { }
