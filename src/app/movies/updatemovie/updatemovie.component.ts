import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiserviceService } from 'src/app/services/apiservice.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-updatemovie',
  templateUrl: './updatemovie.component.html',
  styleUrls: ['./updatemovie.component.css']
})
export class UpdatemovieComponent implements OnInit {

  data: any;

  constructor(private dataService: DataService, private formbuilder: FormBuilder, private apiService: ApiserviceService) { }

  myForm = this.formbuilder.group({
    moviename : ['', Validators.required],
    releasedate : ['', Validators.required],
    actors : ['', Validators.required],
    directors : ['', Validators.required],
    producers : ['', Validators.required],
    rating : ['', Validators.required],
    totalcost : ['', Validators.required],
    description : ['', Validators.required]
  });

  update(){
    if(this.myForm.status == 'VALID'){
      let movie = {
        movieName: this.myForm.value.moviename,
        relaseDate: this.myForm.value.releasedate,
        actors: this.myForm.value.actors,
        directors: this.myForm.value.directors,
        producers: this.myForm.value.producers,
        rating: this.myForm.value.rating,
        totalCost: this.myForm.value.totalcost,
        description: this.myForm.value.description
      }

      this.apiService.updateMovie(movie).subscribe(data => console.log(data));
    }
  }

  ngOnInit(): void {
    this.data = this.dataService.getUpdatedMovieData();
    console.log(this.data);
  }

}
