import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from 'src/app/services/apiservice.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-viewmovie',
  templateUrl: './viewmovie.component.html',
  styleUrls: ['./viewmovie.component.css']
})
export class ViewmovieComponent implements OnInit {

  movieList: any;
  deleteStatus: any;

  constructor(private apiService: ApiserviceService, private dataService: DataService) { }

  updateMovie(movie: any) {
    this.dataService.setUpdatedMovieData(movie);
  }

  deleteMovie(id: any) {
    this.apiService.deleteMovie(id).subscribe(data => {
      this.deleteStatus = data
      if(this.deleteStatus){
        alert('Movie deleted successfully');
      }
      else{
        alert('Movie not found or unable to be delete');
      }
      window.location.reload();
    });
  }

  ngOnInit(): void {
    this.apiService.getAllMovies().subscribe(data => this.movieList = data );
  }

}
